package com.rjz.sharareh.convertnumbertowords;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn_convert = (Button) findViewById(R.id.main_btn_convert);
        final EditText et_inputnum = (EditText) findViewById(R.id.main_et_num);
        final TextView tv_shownum = (TextView) findViewById(R.id.main_tv_showword);
//----------------------------------------------------------------------------------
        final String[] yekan = {"", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine",
                "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
        final String[] dahghan = {"", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
//----------------------------------------------------------------------------------
        btn_convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int index = Integer.parseInt(et_inputnum.getText().toString());

                int firstnum = (int) Math.ceil(index / 10);
                int secoundnum = index % 10;

                if (index == 0) {
                    tv_shownum.setText("Zero");
                } else if (index < 20) {
                    tv_shownum.setText(yekan[index]);
                } else if (index < 100) {
                    tv_shownum.setText(dahghan[firstnum] + yekan[secoundnum]);
                } else if (index < 1000) {
                    int firstnumh = (int) Math.ceil(index / 100);
                    int secoundnumh = index % 100;
                    int thirdnum= (int) Math.ceil(secoundnumh/10);
                    tv_shownum.setText(yekan[firstnumh] +" Hundred " +dahghan[thirdnum]+" "+yekan[secoundnum]);
                }else if(index>999){
                    tv_shownum.setText(" Enter Number Between 0 - 999 ");
                }
            }
        });
    }
}
